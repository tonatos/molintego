#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline
from adminsortable.admin import SortableAdminMixin

from .models import *


def inline_factory(m, parent_class=TabularInline):
    class helper(parent_class):
        model = m
    return helper

class MaterialAdmin(SortableAdminMixin, admin.ModelAdmin):
    inlines = [
        inline_factory(MaterialPhoto),
    ]
    list_display = ('art', 'structure', 'country',)
admin.site.register(Material, MaterialAdmin)

class MaterialCategoryAdmin(SortableAdminMixin, admin.ModelAdmin):
    ordering = ['title',]
    list_display = ('title',)

admin.site.register(MaterialCategory, MaterialCategoryAdmin)