# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0005_auto_20150201_1323'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='material',
            options={'ordering': ('order',), 'verbose_name': '\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b', 'verbose_name_plural': '\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b'},
        ),
        migrations.AddField(
            model_name='material',
            name='order',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438', editable=False),
            preserve_default=True,
        ),
    ]
