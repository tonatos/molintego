# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='structure',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u043e\u0441\u0442\u0430\u0432', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='material',
            name='country',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', blank=True),
            preserve_default=True,
        ),
    ]
