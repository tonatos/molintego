# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0002_auto_20150201_1232'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='material',
            name='title',
        ),
        migrations.AlterField(
            model_name='material',
            name='art',
            field=models.CharField(max_length=255, verbose_name='\u0410\u0440\u0442\u0438\u043a\u0443\u043b'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='material',
            name='photos',
            field=models.ManyToManyField(to='materials.MaterialPhoto', verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438'),
            preserve_default=True,
        ),
    ]
