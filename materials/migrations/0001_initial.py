# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430')),
                ('art', models.CharField(max_length=255, null=True, verbose_name='\u0410\u0440\u0442\u0438\u043a\u0443\u043b', blank=True)),
                ('country', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430', blank=True)),
                ('we_has', models.BooleanField(default=0, verbose_name='\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438')),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b',
                'verbose_name_plural': '\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MaterialCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438')),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MaterialPhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('img', filer.fields.image.FilerImageField(related_name='material_img', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', to='filer.Image')),
            ],
            options={
                'verbose_name': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='material',
            name='category',
            field=models.ManyToManyField(to='materials.MaterialCategory', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='material',
            name='photos',
            field=models.ManyToManyField(to='materials.MaterialPhoto', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438'),
            preserve_default=True,
        ),
    ]
