# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0003_auto_20150201_1235'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='material',
            name='photos',
        ),
        migrations.AddField(
            model_name='materialphoto',
            name='material',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', blank=True, to='materials.Material', null=True),
            preserve_default=True,
        ),
    ]
