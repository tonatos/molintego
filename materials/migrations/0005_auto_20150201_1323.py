# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0004_auto_20150201_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='material',
            name='category',
            field=models.ManyToManyField(related_name='material_items', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', to='materials.MaterialCategory'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='materialphoto',
            name='material',
            field=models.ForeignKey(related_name='material_photos', verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', blank=True, to='materials.Material', null=True),
            preserve_default=True,
        ),
    ]
