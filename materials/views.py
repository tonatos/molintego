#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *

class MaterialListView(ListView):
    model = Material
    context_object_name='items'
    template_name = 'materials/list.html'
    paginate_by = 30

    def get_queryset(self):
        return super(MaterialListView, self).get_queryset().order_by('-we_has', 'order')


class MaterialCategoryDetailView(DetailView):
    model = MaterialCategory
    context_object_name='item'
    template_name = 'materials/category-item.html'

class MaterialDetailView(DetailView):
    model = Material
    context_object_name='item'
    template_name = 'materials/item.html'