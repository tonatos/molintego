#-*- coding: UTF-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from .models import MaterialCategory

class MaterialMenu(CMSAttachMenu):
    name = u'Меню материалов'

    def get_nodes(self, request):
        nodes = []

        for item in MaterialCategory.objects.order_by('order'):
            nodes.append(NavigationNode(item.title,
                                        item.get_absolute_url(),
                                        item.pk))
        return nodes

menu_pool.register_menu(MaterialMenu) # register the menu