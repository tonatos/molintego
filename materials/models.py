#-*- coding: UTF-8 -*-

from django.db import models
from filer.fields.image import FilerImageField



class MaterialCategory(models.Model):
    title = models.CharField(u'Название категории', max_length=255)

    order = models.PositiveIntegerField(u'Порядок сортировки', default=0,
                                        editable=False, blank=False, null=False)

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'
        ordering=('order',)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'material_category_item', [self.pk]

# Create your models here.
class Material(models.Model):
    art = models.CharField(u'Артикул', max_length=255)
    country = models.CharField(u'Страна', max_length=255, blank=True, null=True)
    structure = models.CharField(u'Состав', max_length=255, blank=True, null=True)
    we_has = models.BooleanField(u'В наличии', default=0)

    category = models.ManyToManyField(MaterialCategory, verbose_name=u'Категории',
                                 related_name='material_items')

    order = models.PositiveIntegerField(u'Порядок сортировки', default=0,
                                        editable=False, blank=False, null=False)
    class Meta:
        verbose_name = u'Материал'
        verbose_name_plural = u'Материалы'
        ordering=('order',)

    def __unicode__(self):
        return self.art

    @models.permalink
    def get_absolute_url(self):
        return 'material_item', [self.pk]


class MaterialPhoto(models.Model):
    # consider if you need only one image or multiple images
    img =  FilerImageField(verbose_name=u'Изображение',
                           related_name="material_img")
    material = models.ForeignKey(Material, verbose_name=u'Картинки',
                                 blank=True, null=True,
                                 related_name='material_photos')

    class Meta:
        verbose_name = u'Фотография'
        verbose_name_plural = u'Фотографии'