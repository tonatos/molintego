from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', MaterialListView.as_view()),

    url(r'^category/(?P<pk>\d+)/$', MaterialCategoryDetailView.as_view(),
        name='material_category_item'),

    url(r'^(?P<pk>\d+)/$', MaterialDetailView.as_view(),
        name='material_item'),
)
