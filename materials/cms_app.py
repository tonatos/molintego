#-*- coding: UTF-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import MaterialMenu

class MaterialApphook(CMSApp):
    name = u'Материалы'
    urls = ['materials.urls']
    menus = [MaterialMenu]

apphook_pool.register(MaterialApphook)