# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='solutionitem',
            options={'verbose_name': '\u0423\u0441\u043b\u0443\u0433\u0430', 'verbose_name_plural': '\u0423\u0441\u043b\u0443\u0433\u0438'},
        ),
    ]
