# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0002_auto_20150201_1224'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='solutionitem',
            options={'ordering': ('order',), 'verbose_name': '\u0423\u0441\u043b\u0443\u0433\u0430', 'verbose_name_plural': '\u0423\u0441\u043b\u0443\u0433\u0438'},
        ),
        migrations.AddField(
            model_name='solutionitem',
            name='content',
            field=djangocms_text_ckeditor.fields.HTMLField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='solutionitem',
            name='order',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438', editable=False),
            preserve_default=True,
        ),
    ]
