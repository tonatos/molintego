#-*- coding: UTF-8 -*-
from django.conf.urls import patterns, url

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import SolutionMenu

class SolutionApphook(CMSApp):
    name = u'Услуги'
    urls = ['solution.urls']
    menus = [SolutionMenu]

apphook_pool.register(SolutionApphook)