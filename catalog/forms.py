# -*- coding: utf-8 -*-
from django import forms
from .models import Request

class RequestForm(forms.ModelForm):
    phone = forms.CharField(
        label = 'Телефон',
        widget = forms.widgets.Input(attrs={'class': 'form-control'}),
        required = True )

    email = forms.CharField(
        label = 'Эл. почта',
        widget = forms.widgets.Input(attrs={'class': 'form-control'}),
        required = True )

    subject = forms.CharField(
        label = 'Сообщение',
        widget = forms.widgets.Textarea(attrs={'class': 'form-control', 'rows': 3}),
        required = False )

    class Meta:
        fields = [
            'phone',
            'email',
            'subject',
        ]
        model = Request
        exclude = ('created', 'updated', )

    def __init__(self, *args, **kwargs):
        super(RequestForm, self).__init__(*args, **kwargs)
