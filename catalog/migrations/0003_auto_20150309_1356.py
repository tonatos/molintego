# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_request_item'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catalogcollection',
            options={'ordering': ['order'], 'verbose_name': '\u041a\u043e\u043b\u043b\u0435\u043a\u0446\u0438\u044f', 'verbose_name_plural': '\u041a\u043e\u043b\u043b\u0435\u043a\u0446\u0438\u0438'},
        ),
        migrations.AlterModelOptions(
            name='catalogitem',
            options={'ordering': ['order'], 'verbose_name': '\u0422\u043e\u0432\u0430\u0440', 'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b'},
        ),
        migrations.AlterModelOptions(
            name='catalogtype',
            options={'ordering': ['order'], 'verbose_name': '\u0422\u0438\u043f', 'verbose_name_plural': '\u0422\u0438\u043f\u044b'},
        ),
        migrations.AddField(
            model_name='catalogcollection',
            name='order',
            field=models.PositiveIntegerField(default=1, editable=False, db_index=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='catalogitem',
            name='order',
            field=models.PositiveIntegerField(default=1, editable=False, db_index=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='catalogtype',
            name='order',
            field=models.PositiveIntegerField(default=1, editable=False, db_index=True),
            preserve_default=True,
        ),
    ]
