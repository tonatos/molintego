# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0004_auto_20150309_1412'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catalogitem',
            options={'ordering': ('-we_has', 'title'), 'verbose_name': '\u0422\u043e\u0432\u0430\u0440', 'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b'},
        ),
        migrations.AlterField(
            model_name='catalogcollection',
            name='order',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='catalogtype',
            name='order',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438', editable=False),
            preserve_default=True,
        ),
    ]
