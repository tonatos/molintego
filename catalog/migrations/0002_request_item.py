# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='request',
            name='item',
            field=models.ForeignKey(default=None, to='catalog.CatalogItem', blank=True, null=True),
            preserve_default=False,
        ),
    ]
