# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20150309_1356'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catalogcollection',
            options={'ordering': ('order',), 'verbose_name': '\u041a\u043e\u043b\u043b\u0435\u043a\u0446\u0438\u044f', 'verbose_name_plural': '\u041a\u043e\u043b\u043b\u0435\u043a\u0446\u0438\u0438'},
        ),
        migrations.AlterModelOptions(
            name='catalogitem',
            options={'ordering': ('type__id', '-we_has', 'title'), 'verbose_name': '\u0422\u043e\u0432\u0430\u0440', 'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b'},
        ),
        migrations.AlterModelOptions(
            name='catalogtype',
            options={'ordering': ('order',), 'verbose_name': '\u0422\u0438\u043f', 'verbose_name_plural': '\u0422\u0438\u043f\u044b'},
        ),
        migrations.RemoveField(
            model_name='catalogitem',
            name='order',
        ),
        migrations.AddField(
            model_name='catalogitem',
            name='we_has',
            field=models.BooleanField(default=0, verbose_name='\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='catalogcollection',
            name='order',
            field=models.PositiveIntegerField(default=1, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='catalogtype',
            name='order',
            field=models.PositiveIntegerField(default=1, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438', editable=False),
            preserve_default=True,
        ),
    ]
