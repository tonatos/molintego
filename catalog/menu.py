#-*- coding: UTF-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from .models import CatalogType, CatalogCollection, CatalogItem

class CatalogMenu(CMSAttachMenu):
    name = u'Меню каталога'

    def get_nodes(self, request):
        nodes = []

        for item in CatalogType.objects.all():
            nodes.append(NavigationNode(item.title,
                                        item.get_absolute_url(),
                                        'type-%s' % item.pk))
            for i in CatalogItem.objects.filter(type=item):
                nodes.append(NavigationNode(i.title,
                                            i.get_absolute_url(),
                                            'type-item-%s' % i.pk))

        for item in CatalogCollection.objects.all():
            nodes.append(NavigationNode(item.title,
                                        item.get_absolute_url(),
                                        'collection-%s' % item.pk))
            for i in CatalogItem.objects.filter(collection=item):
                nodes.append(NavigationNode(i.title,
                                            i.get_absolute_url(),
                                            'collection-item-%s' % i.pk))

        return nodes

menu_pool.register_menu(CatalogMenu) # register the menu