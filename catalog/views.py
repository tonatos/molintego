#-*- coding: UTF-8 -*-
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.views.generic.base import ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.models import User

from .forms import *
from .models import *

class FilterMixin(ContextMixin):
    def get_context_data(self, *args, **kwargs):
        context = super(FilterMixin, self).get_context_data(*args, **kwargs)
        context['collection'] = CatalogCollection.objects.order_by('order')
        context['type'] = CatalogType.objects.order_by('order')
        context['request_pk'] = self.kwargs
        return context

class CatalogListView(ListView, FilterMixin):
    model = CatalogItem
    context_object_name='items'
    template_name = 'catalog/list.html'
    paginate_by = 30

    def get_queryset(self):
        return super(CatalogListView, self).get_queryset().order_by('order')


class CatalogTypeDetailView(DetailView, FilterMixin):
    model = CatalogType
    context_object_name='item'
    template_name = 'catalog/catalog-type-item.html'
    pk_url_kwarg = 'type_pk'

    def get_queryset(self):
        return super(CatalogTypeDetailView, self).get_queryset().order_by('order')

class CatalogCollectionDetailView(DetailView, FilterMixin):
    model = CatalogCollection
    context_object_name='item'
    template_name = 'catalog/catalog-collection-item.html'
    pk_url_kwarg = 'collection_pk'

    def get_queryset(self):
        return super(CatalogCollectionDetailView, self).get_queryset().order_by('order')

class CatalogItemlDetailView(DetailView):
    model = CatalogItem
    context_object_name='item'
    template_name = 'catalog/item.html'


    def get_context_data(self, *args, **kwargs):
        context = super(CatalogItemlDetailView, self).get_context_data(*args, **kwargs)
        context['form'] = RequestForm()
        return context
    

def request_form(request, pk=None):
    obj = get_object_or_404(CatalogItem, pk=pk)

    if request.method == 'POST':
        request_form = RequestForm(data = request.POST)

        if request_form.is_valid():
            request_item = request_form.save(commit = False)
            request_item.item = obj
            request_item.save()

            try:
                email = User.objects.filter(is_superuser = True)[0].email
            except ValueError:
                email = settings.MAIN_EMAIL

            message = render_to_string(
                    'email/email_request_form.txt',
                    {
                        'request_item': request_item,
                        'url': request.build_absolute_uri(
                            reverse('admin:catalog_request_change', args=[request_item.pk])
                        )
                    }
                )
            send_mail(u'Заказ с сайта «Molintego»!', message, 'no-reply@molintego.ru',
                      settings.EMAILS + [email])

            return render(request, 'catalog/__request_form.html', {
                'form': RequestForm(),
                'ok': True
            })
    else:
        request_form = RequestForm()

    return render(request, 'catalog/__request_form.html', {'form': request_form})
