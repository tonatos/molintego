#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline
from adminsortable.admin import SortableAdminMixin

from .models import *


def inline_factory(m, parent_class=TabularInline):
    class helper(parent_class):
        model = m
    return helper

class CatalogItemAdmin(SortableAdminMixin, admin.ModelAdmin):
    inlines = [
        inline_factory(MaterialPhoto),
    ]
    list_display = ('title',)
    ordering=('order',)

admin.site.register(CatalogItem, CatalogItemAdmin)

class CatalogCategoryAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('title','order',)

class CatalogTypeAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('title','order',)

class RequestAdmin(admin.ModelAdmin):
    ordering = ['-created',]
    list_display = ('created', 'phone', 'email', )
    fieldsets = [
        ('Данные', {
            'fields': [
                'phone',
                'email',
                'subject',
                'object_field',
                'created',]
        })
    ]
    readonly_fields = ['object_field', 'created']

    def object_field(self, obj):
        return '<a href="%s">%s</a>' % (
            obj.item.get_absolute_url(), obj.item.title
        )
    object_field.allow_tags = True
    object_field.short_description = u'Модель'

admin.site.register(CatalogType, CatalogTypeAdmin)
admin.site.register(CatalogCollection, CatalogCategoryAdmin)
admin.site.register(Request, RequestAdmin)