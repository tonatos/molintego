#-*- coding: UTF-8 -*-

import datetime
from django.db import models

# Create your models here.
from djangocms_text_ckeditor.fields import HTMLField

from filer.fields.image import FilerImageField

class CatalogType(models.Model):
    title = models.CharField(u'Название категории', max_length=255)
    order = models.PositiveIntegerField(u'Порядок сортировки', default=0,
                                        editable=False, blank=False, null=False)

    class Meta:
        verbose_name = u'Тип'
        verbose_name_plural = u'Типы'
        ordering=('order',)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'catalog_type_item', [self.pk]


class CatalogCollection(models.Model):
    title = models.CharField(u'Название коллекции', max_length=255)
    order = models.PositiveIntegerField(u'Порядок сортировки', default=0,
                                        editable=False, blank=False, null=False)

    class Meta:
        verbose_name = u'Коллекция'
        verbose_name_plural = u'Коллекции'
        ordering=('order',)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'catalog_collection_item', [self.pk]


class CatalogItem(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)
    
    description = HTMLField(u'Описание', blank=True, null=True)

    type = models.ManyToManyField(CatalogType, verbose_name=u'Тип',
                                 related_name='catalog_type')

    collection = models.ManyToManyField(CatalogCollection, verbose_name=u'Коллекция',
                                 related_name='catalog_collection')

    we_has = models.BooleanField(u'В наличии', default=0)

    order = models.PositiveIntegerField(u'Порядок сортировки', default=0,
                                        editable=False, blank=False, null=False)

    class Meta:
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'
        ordering=('order',)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'catalog_item', [self.pk]


class MaterialPhoto(models.Model):
    # consider if you need only one image or multiple images
    img =  FilerImageField(verbose_name=u'Изображение',
                           related_name="catalog_img")

    material = models.ForeignKey(CatalogItem, verbose_name=u'Картинки',
                                 blank=True, null=True,
                                 related_name='catalog_photos')

    class Meta:
        verbose_name = u'Фотография'
        verbose_name_plural = u'Фотографии'


class Request(models.Model):
    phone = models.CharField(u'Телефон', max_length=255)
    email = models.EmailField(u'E-mail', max_length=255)
    subject = models.TextField(u'Сообщение', max_length=1000)

    created = models.DateTimeField(u'Дата добавления',
                                   auto_now=False, auto_now_add=True, null=True,
                                   blank = False, default = datetime.datetime.now)

    updated = models.DateTimeField(u'Дата изменения', auto_now=True, auto_now_add=True,
                                   blank = False, default = datetime.datetime.now)

    item = models.ForeignKey(CatalogItem, blank=True, null=True)

    def __unicode__(self):
        return str(self.created)

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'
        ordering = ['-created', ]