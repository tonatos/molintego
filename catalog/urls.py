#-*- coding: UTF-8 -*-

from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', CatalogListView.as_view()),

    url(r'^type/(?P<type_pk>\d+)/$', CatalogTypeDetailView.as_view(),
        name='catalog_type_item'),

    url(r'^collection/(?P<collection_pk>\d+)/$', CatalogCollectionDetailView.as_view(),
        name='catalog_collection_item'),

    url(r'^(?P<pk>\d+)/$', CatalogItemlDetailView.as_view(),
        name='catalog_item'),

    url(r'^request_form/(?P<pk>\d+)/', 'catalog.views.request_form', name='request_form'),
)