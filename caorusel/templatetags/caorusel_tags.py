#-*- coding: UTF-8 -*-
from django import template

from caorusel.models import *

register = template.Library()

@register.inclusion_tag('caorusel/banner.html')
def caorusel_banner():
    return {'items': Caorusel.objects.all() }