from django.contrib import admin
from .models import Caorusel

class CaoruselAdmin(admin.ModelAdmin):
    ordering = ['title',]
    list_display = ('title',)
admin.site.register(Caorusel, CaoruselAdmin)