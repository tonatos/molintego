#-*- coding: UTF-8 -*-

from django.db import models

# Create your models here.
from filer.fields.image import FilerImageField


class Caorusel(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)

    url = models.URLField(u'Ссылка', null=True, blank=True)

    img =  FilerImageField(verbose_name=u'Изображение',
                           related_name="caorusel_img", help_text=u'Размер картинки должен быть 1115x400')

    order = models.IntegerField(u'Порядок сортировки', default=0, blank=True)

    class Meta:
        ordering = ['order']
        verbose_name = 'Банер'
        verbose_name_plural = 'Баннеры'