#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *

class NewsItemListView(ListView):
    model = NewsItem
    context_object_name='items'
    template_name = 'news/list.html'
    paginate_by = 20

    def get_queryset(self):
        return super(NewsItemListView, self).get_queryset().order_by('-created')


class NewsItemDetailView(DetailView):
    model = NewsItem
    context_object_name='item'
    template_name = 'news/item.html'