#-*- coding: UTF-8 -*-
from django import template
from news.models import NewsItem

register = template.Library()

@register.inclusion_tag('news/tags/ref_news.html')
def ref_news(request, exclude=None):
    return {'news': NewsItem.objects\
                        .exclude(pk=exclude)\
                        .order_by('-created')[:2]}

#@register.inclusion_tag('/news/tags/my_tag.html', takes_context=True)
#def my_tag_with_context(context):
#    pass