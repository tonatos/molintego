# -*- coding: utf-8 -*-
from django import forms
from .models import FeedbackSubject

class FeedbackSubjectForm(forms.ModelForm):
    name = forms.CharField(
        label = 'Ваше имя',
        widget = forms.widgets.Input(attrs={'class': 'form-control'}),
        required = True )

    phone = forms.CharField(
        label = 'Телефон',
        widget = forms.widgets.Input(attrs={'class': 'form-control'}),
        required = True )

    email = forms.CharField(
        label = 'Эл. почта',
        widget = forms.widgets.Input(attrs={'class': 'form-control'}),
        required = True )

    subject = forms.CharField(
        label = 'Сообщение',
        widget = forms.widgets.Textarea(attrs={'class': 'form-control', 'rows': 3}),
        required = False )

    class Meta:
        fields = [
            'name',
            'phone',
            'email',
            'subject',
        ]
        model = FeedbackSubject
        exclude = ('created', 'updated', )

    def __init__(self, *args, **kwargs):
        super(FeedbackSubjectForm, self).__init__(*args, **kwargs)
