#-*- coding: UTF-8 -*-
from itertools import izip_longest

from django import template


register = template.Library()
@register.filter('grouper')
def grouper_filter(value, arg):
    # http://docs.python.org/library/itertools.html
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    izip_args = [iter(value)] * arg
    ret = list(izip_longest(fillvalue=None, *izip_args))
    return ret