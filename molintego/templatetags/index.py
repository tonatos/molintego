#-*- coding: UTF-8 -*-
from django import template
from molintego.forms import FeedbackSubjectForm

from news.models import *

register = template.Library()

@register.inclusion_tag('index/tmp.html')
def get_directions():
    return {}


@register.inclusion_tag('parts/news.html')
def get_news():
    return {'items': NewsItem.objects.filter()[:3] }

@register.inclusion_tag('parts/feedback_form.html')
def feedback_form(request):
    return {'form': FeedbackSubjectForm(), 'request': request }