
$(document).ready(function(){
    $('.filter .toggle').click(function(){
        $obj = $('#filter-data');

        if ( $obj.css('display') == 'block' ) {
            $obj.hide();
        } else {
            $obj.show();
        }
    });

    $('.preview_list a').click(function(){
        $('.preview_list a').removeClass('active');
        $(this).addClass('active');
        $('.main-img-img').attr('src', $(this).data('target'));
        return false;
    });

    $("#requestForm").parent().submit(function(){
        $.post(
            $(this).attr('action'),
            $(this).serialize(),
            function(data){
                $("#requestForm").html(data)
            });
        return false;
    })
})